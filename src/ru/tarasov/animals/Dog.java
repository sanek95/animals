package ru.tarasov.animals;

/**
 * Created by Администратор on 16.10.2014.
 */
public class Dog implements Animals{

    @Override
    public String getName() {
        return "Собака";
    }

    @Override
    public String getVoice() {
        return "гав-гав";
    }
}
